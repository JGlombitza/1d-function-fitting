"""
Fully connected network for 1D function fitting
Run this script with 'pygpu train_1D_fit.py'
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import tensorflow as tf
from tensorflow import keras
layers = keras.layers

# ---------------------------------------------------------
# Boilerplate. You can ignore this part.
# ---------------------------------------------------------
try:
    CONDOR_ID = os.environ['CONDOR_ID']
except KeyError:
    sys.exit('Error: Run this script with "pygpu %file"')
tf.logging.set_verbosity(tf.logging.ERROR)

folder = 'train-%s' % CONDOR_ID  # folder for training results
os.makedirs(folder)


# -----------------------------------------------------------
# Generate Data
# -----------------------------------------------------------
def f(x):
    """ Some complicated function """
    return ((np.abs(x))**0.5 + 0.1*x + 0.01*x**2 + 1 - np.sin(x) + 0.5*np.exp(x/10.)) / (0.5+np.abs(np.cos(x)))


N = 10**4  # number of training samples
xdata = np.linspace(-10, 10, N)[:, None]  # Note: "[:,None]" reshapes to (N,1) as required by our DNN
ydata = f(xdata)


# -----------------------------------------------------------
# Settings
# -----------------------------------------------------------
nb_layers = 3       # number of hidden layers
nb_nodes = 20       # number of nodes per layer
activation = 'relu'  # activation function
epochs = 400      # number of training epochs
save_period = 80   # number of epochs between epochs of network

# (additional) input features to train on
xtrain = xdata
# xtrain = np.c_[ xdata, xdata**2, np.sin(xdata), np.cos(xdata)]  # some extra features


# -----------------------------------------------------------
# Define model
# -----------------------------------------------------------
model = keras.models.Sequential()
model.add(layers.Dense(nb_nodes, activation=activation, input_dim=xtrain.shape[1]))
for i in range(nb_layers-1):
    model.add(layers.Dense(nb_nodes, activation=activation))
model.add(layers.Dense(1))

print(model.summary())


# -----------------------------------------------------------
# Training
# -----------------------------------------------------------

model.compile(loss='mean_squared_error', optimizer='adam')

results = model.fit(xtrain, ydata, batch_size=128, epochs=epochs, verbose=1,
                    callbacks=[keras.callbacks.CSVLogger(folder + '/history.csv'),
                               keras.callbacks.ModelCheckpoint(folder+'/weights-{epoch:02d}.hdf5', save_weights_only=True, period=save_period)])

# -----------------------------------------------------------
# Plot
# -----------------------------------------------------------
fig, (ax1, ax2) = plt.subplots(nrows=2, figsize=(12, 8))
plt.tight_layout()

# plot data and fit
ax1.plot(xdata, ydata, color='black', label='data')
epochs = range(save_period, epochs+1, save_period)

colors = [plt.cm.jet((i+1) / float(len(epochs)+1)) for i in range(len(epochs))]

for i, epoch in enumerate(epochs):
    model.load_weights('%s/weights-%i.hdf5' % (folder, epoch))
    ypredict = model.predict(xtrain).T[0]
    ax1.plot(xdata, ypredict, color=colors[i], label=epoch)
    ax2.plot(epoch, results.history['loss'][epoch-1], color=colors[i], marker='o')

ax1.set(xlabel='x', ylabel='f(x)', xlim=(-10, 13), title='%i layers, %i nodes, %s' % (nb_layers, nb_nodes, activation))
ax1.grid(True)
ax1.legend(loc='upper right', title='Epochs')

# plot history of loss
ax2.plot(results.history['loss'], color='black')
ax2.set(xlabel='epoch', ylabel='loss')
ax2.grid(True)
ax2.semilogy()

fig.savefig(folder + '/fit.png', bbox_inches='tight')
